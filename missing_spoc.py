"""================================================
Version: 1.1
Author: Rohan D
Description: Program to generate report of missing SPOCs or count Hosts from Zabbix.
================================================"""


import argparse
import os
import requests
from zabbix.login import Login
from zabbix.hostgroup import Hostgroup
from zabbix.host import Host
import re
import csv
import pandas as pd
from tqdm import tqdm

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--get_spocs", help="Generate Missing SPOCs report", action="store_true")
    parser.add_argument("--count_hosts", help="Get count of Hosts for all Hostgroups", action="store_true")
    return parser

def get_vault_spocs(file_path, customers):

    with open(os.path.join(file_path, 'missing_spocs.csv'), 'w', newline='') as csvFile:    #Write Missing SPOCs CSV File
        writer = csv.DictWriter(csvFile, fieldnames=['Customer', 'App', 'KV Present', 'SPOC Present', 'SPOC'])
        writer.writeheader()
        for index, row in tqdm(customers.iterrows(), total=customers.shape[0], desc="Fetching SPOC details"):

            row_dict = {'Customer':'-', 'App':'-', 'KV Present':'-', 'SPOC Present':'-', 'SPOC':'-'}

            url = f"{os.environ['VAULT_KV_URL']}{row['Customer']}/spoc"
            headers = {'Content-Type': 'application/json', 'X-Vault-Token': os.environ['VAULT_TOKEN']}
            response = requests.get(url, headers= headers)    # Fetch SPOC details from vault

            row_dict['Customer'] = row['Customer']
            if response.status_code == 404:    # KV not found
                row_dict['KV Present'] = 'No'
                writer.writerow(row_dict)
            elif response.status_code == 200:  # KV found
                row_dict['KV Present'] = 'Yes'
                Apps = row['Apps']
                Spocs = response.json()['data']
                for app in Apps:
                    row_dict['App'] = app
                    if app in Spocs.keys():
                        row_dict['SPOC Present'] = 'Yes'
                        row_dict['SPOC'] = Spocs[app]
                    elif app == 'No-App':
                        row_dict['SPOC Present'] = 'No App found'
                    else:
                        row_dict['SPOC Present'] = 'No'
                    writer.writerow(row_dict)
            # Any other response - 403: Vault Token ,500: Error in API call ,502: Vault Down 
            else:
                row_dict['Customer'] = f"Error Code - {response.status_code}"
                writer.writerow(row_dict)
    return

def zbx_hostgroups():

    ZABBIX_URL = "https://mon.ccp.capgemini.com/api_jsonrpc.php"

    login = Login(ZABBIX_URL)
    login_data = login.login( {'user': os.environ['ZABBIX_USER'], 'password': os.environ['ZABBIX_PASSWORD']}, None, 0 )    # Login to Zabbix

    all_dict = {}
    if login_data['result']:
        hg = Hostgroup(ZABBIX_URL)
        hostgroups = hg.get(params={"output":"extend"}, auth=login_data['result'], id=1)    # Fetch all Zabbix Hostgroups
        if hostgroups['result']:
            patterns=r"^templates|^windows servers|^discovered hosts|^new server builds|^to be decommissioned|^exception"    # Ignore these patterns, sample hostgroups
            for host in hostgroups['result']:
                if not re.search(patterns, host['name'].strip().lower()):

                    cus = host['name'].strip().split('/')[0]
                    try:
                        app = host['name'].split('/')[4]
                    except:
                        app = "No-App"
                    
                    if cus in all_dict:
                        all_dict[cus].add(app)
                    else:
                        all_dict[cus] = {app}

            df = pd.DataFrame(list(all_dict.items()), columns = ['Customer','Apps'])
            return df
        else:
            print("Hostgroups data from Zabbix not obtained.")
            return
    else:
        print("Zabbix Login Failed")
        return
    
def count_hosts(project_path):

    ZABBIX_URL = "https://mon.ccp.capgemini.com/api_jsonrpc.php"
    login = Login(ZABBIX_URL)
    login_data = login.login( {'user': os.environ['ZABBIX_USER'], 'password': os.environ['ZABBIX_PASSWORD']}, None, 0 )    # Login to Zabbix

    if login_data['result']:
        hg = Hostgroup(ZABBIX_URL)
        hostgroups = hg.get(params={"output":"extend"}, auth=login_data['result'], id=1)    # Fetch all Zabbix Hostgroups
        all_hostgroups = []
        if hostgroups['result']:
            patterns=r"^templates|^windows servers|^discovered hosts|^new server builds|^to be decommissioned|^exception"
            with open(os.path.join(project_path, 'count_hosts.csv'), 'w', newline='') as csvFile:
                writer = csv.DictWriter(csvFile, fieldnames=['Hostgroup', 'Hosts'])
                writer.writeheader()
                for hostgroup in tqdm(hostgroups['result'], total= len(hostgroups['result'])):
                    row_dict = {"Hostgroup":"-", "Hosts":"-"}
                    if not re.search(patterns, hostgroup['name'].strip().lower()):
                        h = Host(ZABBIX_URL)
                        hosts = h.get(params={"output":"extend","groupids":hostgroup['groupid']}, auth=login_data['result'], id=1)    # Fetch all Hosts for given Hostgroup
                        row_dict["Hostgroup"] = hostgroup['name'].strip()
                        row_dict["Hosts"] = len(hosts['result'])
                        writer.writerow(row_dict)
            return
        else:
            print("Hostgroups data from Zabbix not obtained.")
            return
    else:
        print("Zabbix Login Failed")
        return


def main():
    project_path = os.path.dirname(os.path.abspath(__file__))
    
    parser = parse_arguments()
    args = parser.parse_args()
    if args.get_spocs:
        hostgroups = zbx_hostgroups()
        if not hostgroups.empty:
            get_vault_spocs(project_path, hostgroups)
    elif args.count_hosts:
        count_hosts(project_path)
    else:
        parser.error("Please specify an argument.")

    return

if __name__ == "__main__":
    main()