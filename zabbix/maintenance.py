import json
import requests

class Maintenance:
    def __init__(self, url, headers={"content-type": "application/json"}):
        self.url = url
        self.headers = headers
        self.payload = {}

    def create(self, params, auth, id):
        self.payload = {"jsonrpc": "2.0", "method": "maintenance.create", "params": params, "auth": auth, "id": id}
        response = requests.post(url=self.url, data=json.dumps(self.payload), headers = self.headers)
        json_output = response.json()
        return json_output

    def delete(self, auth, id, params):
        self.payload = {"jsonrpc": "2.0", "method": "maintenance.delete", "params": params, "auth": auth, "id":id}
        response = requests.post(url=self.url, data=json.dumps(self.payload), headers = self.headers)
        json_output = response.json()
        return json_output

    def get_status(self,params, auth, id):
        self.payload = {"jsonrpc": "2.0", "method": "maintenance.get", "params": params, "auth": auth, "id": id}
        response = requests.post(url=self.url, data=json.dumps(self.payload), headers=self.headers)
        json_output = response.json()
        return json_output