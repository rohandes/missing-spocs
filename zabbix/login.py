import requests
import json

class Login:
    def __init__(self, url, headers={"content-type": "application/json"}):
        self.url = url
        self.headers = headers
        self.payload = {}

    def login(self, params, auth, id):
        self.payload = {"jsonrpc": "2.0", "method": "user.login", "params": params, "auth": auth, "id": id}
        response = requests.post( url=self.url, data=json.dumps(self.payload), headers=self.headers)
        login_data = response.json()
        return login_data