import json
import requests

class Graph:
    def __init__(self, url, headers={'content-type': 'application/json'}):
        self.url = url
        self.headers = headers
        self.payload = {}

    def get(self, params, auth, id):
        self.payload = {"jsonrpc": "2.0", "method": "graph.get", "params": params, "auth": auth, "id": id}
        response = requests.post(url=self.url, data=json.dumps(self.payload), headers=self.headers)
        graph_details = response.json()
        return graph_details

