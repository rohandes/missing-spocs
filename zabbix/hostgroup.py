import json
import requests

class Hostgroup:
    def __init__(self, url, headers={'content-type': 'application/json'}):
        self.url = url
        self.headers = headers
        self.payload = {}

    def get(self, params, auth, id):
        self.payload = {"jsonrpc": "2.0", "method": "hostgroup.get", "params": params, "auth": auth, "id": id}
        response = requests.post(url=self.url, data=json.dumps(self.payload), headers=self.headers)
        hostgroup_details = response.json()
        return hostgroup_details
