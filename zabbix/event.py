import requests
import json 
class Event:
    def __init__(self, url, headers={"content-type": "application/json"}):
        self.url = url
        self.headers = headers
        self.payload = {}

    def get_time_series_data(self,  params, auth, id):
        self.payload = {"jsonrpc": "2.0", "method": "event.get", "params": params, "auth": auth, "id": id}
        response = requests.post( url=self.url, data=json.dumps(self.payload), headers=self.headers)
        time_series_data = response.json()
        return time_series_data
