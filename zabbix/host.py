import json
import requests

class Host:
    def __init__(self, url, headers={'content-type': 'application/json'}):
        self.url = url
        self.headers = headers
        self.payload = {}

    def get(self, params, auth, id):
        self.payload = {"jsonrpc": "2.0", "method": "host.get", "params": params, "auth": auth, "id": id}
        response = requests.post(url=self.url, data=json.dumps(self.payload), headers=self.headers)
        host_details = response.json()
        return host_details
