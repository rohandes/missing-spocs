import requests
import json

class History:
    def __init__(self, url, headers={"content-type": "application/json"}):
        self.url = url
        self.headers = headers
        self.payload = {}

    def get_history_data(self, params, auth, id):
        self.payload = {"jsonrpc": "2.0", "method": "history.get", "params": params, "auth": auth, "id": id}
        response = requests.post( url=self.url, data=json.dumps(self.payload), headers=self.headers)
        history_data = response.json()
        return history_data

